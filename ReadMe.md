### Olá sou MathLopes29 👋
#### Criei este perfil para publicar meus projetos e meus estudos.
#### 🔭 No momento estou procurando meu primeiro emprego! 

- 🎒 Estudando Desenvolvimento de Software (FATEC) 
- 📫 Contate-me no email: mat.lopes2907@gmail.com


    <div align="center">
       <img height="175em" src="https://github-readme-stats.vercel.app/api?username=MathLopes29&show_icons=true&theme=gotham&include_all_commits=true&count_private=true"/>
       <img height="175em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=MathLopes29&layout=compact&langs_count=7&theme=gotham"/>
       <div style="display: inline_block"><br>
       <img align="center" alt="Math-Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
       <img align="center" alt="Math-Ts" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-plain.svg">
       <img align="center" alt="Math-React" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
       <img align="center" alt="Math-HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
       <img align="center" alt="Math-CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
       <img align="center" alt="Math-Python" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
       <img align="center" alt="Math-Csharp" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/csharp/csharp-original.svg">
   </div>
    
    
  <div>
  
    <a href= "https://www.instagram.com/math_lopes29/" target="_blank"><img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
    <a href= "https://github.com/MathLopes29" target= "_blank"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white" target="_black"></a> 
     <a href="https://www.linkedin.com/in/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
      
  </div>
 
